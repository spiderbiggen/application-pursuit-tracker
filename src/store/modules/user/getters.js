import * as R from 'ramda';

export function isAuthorized(state) {
  return !!state.token;
}

export function token(state) {
  return state.token;
}

export function state(state) {
  return state.state;
}

export function profile(state) {
  return state.profile;
}

export function memberships(state) {
  return state.memberships;
}

export function characters(state) {
  return R.path(['characters', 'data'], state.profile)
}
