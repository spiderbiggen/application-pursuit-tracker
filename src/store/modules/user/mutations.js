export function loginSuccess(state, payload) {
  state.token = payload.token;
}

export function profile(state, payload) {
  state.profile = payload;
}

export function memberships(state, payload) {
  state.memberships = payload;
}

export function state(state, payload) {
  state.state = payload;
}

export function selectedMembership(state, payload) {
  state.selectedMembership = payload;
}

export function addCharacter(state, { id, character }) {
  if (!id) return;
  state.profile.characters.data[id] = character;
}
