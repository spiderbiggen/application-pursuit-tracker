import { generateRandomString } from '@/lib/StringUtils';
import { BUNGIE_CLIENT_ID } from '@/constants';
import request from '@/lib/request';
import * as R from 'ramda';

export function authorize({ commit }) {
  const state = generateRandomString(16);
  commit('state', state);

  let url = 'https://www.bungie.net/en/OAuth/Authorize';
  url += '?response_type=code';
  url += '&client_id=' + encodeURIComponent(BUNGIE_CLIENT_ID);
  url += '&state=' + encodeURIComponent(state);
  window.location.assign(url);
}

export function logout({dispatch}) {
  dispatch('reset', null, { root: true });
}

export async function verifyAuthentication({ commit, getters }, { code, state }) {
  if (!code || state !== getters.state) return;
  const response = await request.post('users/authenticate', { code });
  commit('loginSuccess', response.data);
}

export async function getSelf({ commit, dispatch }) {
  const response = await request.get('users/me');
  commit('memberships', response.data);
  if (response.data.destinyMemberships && response.data.destinyMemberships.length === 1) {
    const m = response.data.destinyMemberships[0];
    dispatch('profile', { type: m.membershipType, id: m.membershipId });
  }
}

/*
function openLocalForage() {
    let request = window.indexedDB.open('localforage', 2);
	let db;
	return new Promise((resolve, reject) => {
        request.onsuccess = (e) => {
            db = e.target.result;
            let objStore = db.transaction(['keyvaluepairs'], 'readonly').objectStore('keyvaluepairs');
            let r = objStore.get('pursuit-tracker');
            r.onsuccess = (e) => {
				resolve(e.target.result);
            }
        }
    });
};
 */

export async function profile({ commit, state }, { type = state.selectedMembership.type, id = state.selectedMembership.id } = {}) {
  const response = await request.get(`destiny2/${ type }/profile/${ id }`);
  const obj = {};
  obj.characters = response.data.characters;
  commit('selectedMembership', { type, id });
  commit('profile', obj);
  const equipment = R.path(['data', 'characterEquipment', 'data'], response);
  const inventories = R.path(['data', 'characterInventories', 'data'], response);
  Object.keys(equipment).forEach(key => inventories[key].items = R.concat(inventories[key].items || [], equipment[key].items));
  commit('items/items', inventories, { root: true });
  const itemComponents = R.pathOr({}, ['data', 'itemComponents'], response);
  commit('items/instances', itemComponents.instances, { root: true });
  commit('items/objectives', itemComponents.objectives, { root: true });
}

export async function character({ commit, state }, { type = state.selectedMembership.type, memberId = state.selectedMembership.id, characterId } = {}) {
  const response = await request.get(`destiny2/${ type }/profile/${ memberId }/character/${ characterId }`);
  commit('selectedMembership', { type, id: memberId });
  const equipment = R.path(['data', 'equipment', 'data', 'items'], response);
  const inventories = R.path(['data', 'inventory', 'data', 'items'], response);
  commit('addCharacter', response.data.character);
  commit('items/addItems', { id: characterId, items: R.concat(inventories, equipment) }, { root: true });
  const itemComponents = R.pathOr({}, ['data', 'itemComponents'], response);
  commit('items/addInstances', itemComponents.instances, { root: true });
  commit('items/addObjectives', itemComponents.objectives, { root: true });
}

export async function refresh({ dispatch }, route = {}) {
  try {
    if (route.name === 'character' && route.params.id) {
      await dispatch('character', { characterId: route.params.id });
    } else {
      await dispatch('profile');
    }
  } catch (e) {
    console.error(e);
  }
}

export async function manifest() {
  await request.get('destiny2/manifest', { timeout: 0 });
}
