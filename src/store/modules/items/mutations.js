import * as R from 'ramda';

export function items(state, payload) {
  state.items = payload;
}

export function instances(state, payload) {
  state.instances = payload;
}

export function objectives(state, payload) {
  state.objectives = payload;
}


export function addItems(state, { id, items }) {
  if (!id) return;
  state.items[id].items = items;
}

export function addInstances(state, payload) {
  state.instances = R.mergeRight(state.instances, payload);
}

export function addObjectives(state, payload) {
  state.objectives = R.mergeRight(state.objectives, payload);
}

export function trait(state, trait) {
  state.trait = trait;
}
