export async function setTrait({ commit }, trait) {
  commit('trait', trait);
}
