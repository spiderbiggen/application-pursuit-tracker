export default {
  items: {},
  instances: {},
  objectives: {},
  /**
   * Possible traits on 2020-03-25
   *
   * 'inventory_filtering.quest'
   * 'quest.past'
   * 'quest.current_release'
   * 'quest.exotic'
   * 'inventory_filtering.bounty'
   * 'item_type.bounty'
   * 'quest.playlists'
   * 'quest.seasonal'
   */
  trait: null,
};
