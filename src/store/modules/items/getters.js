import * as R from 'ramda';
import { traitFilter } from '@/lib/constants';

export function items(state, getters) {
  return (charId) => {
    const items = getters.unfilteredItems(charId);
    if (!state.trait) return items;
    const filter = traitFilter[state.trait] || ((ids) => ids.includes(state.trait));
    return items.filter(item => {
      const traitIds = R.pathOr([], ['definition', 'traitIds'], item)
      return filter(traitIds);
    });
  };
}

export function unfilteredItems(state) {
  return (charId) => R.pathOr([], [charId, 'items'], state.items).filter(item => item.definition);
}

export function instance(state) {
  return (id) => state.instances.data[id];
}

export function objective(state) {
  return (id) => state.objectives.data[id];
}

export function trait(state) {
  return state.trait;
}

export function activeObjectiveTypes(state, getters) {
  return (charId) => {
    const items = getters.unfilteredItems(charId);
    const objectives = items.map(a => {
      a.objectives = getters.incompleteObjectives(a.itemInstanceId);
      return a;
    }).filter(a => a.objectives && a.objectives.length > 0);
    const traits = new Set();
    objectives.forEach((cur) => (cur.definition.traitIds || []).forEach(t => traits.add(t)));
    return Array.from(traits);
  };
}

export function incompleteObjectives(state, getters) {
  return (id) => R.pathOr([], ['objectives'], getters.objective(id)).filter(objective => !objective.complete && objective.visible);
}
