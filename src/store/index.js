import Vue from 'vue';
import Vuex from 'vuex';
import VuexPersist from 'vuex-persist';
import modules from './modules';
import localforage from 'localforage';

Vue.use(Vuex);

const vuexLocalStorage = new VuexPersist({
  key: 'pursuit-tracker', // The key to store the state on in the storage provider.
  storage: window.localStorage, // or window.sessionStorage or localForage
  modules: ['user'],
  // Function that passes the state and returns the state with only the objects you want to store.
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
});

const vuexLocalForage = new VuexPersist({
  key: 'pursuit-tracker', // The key to store the state on in the storage provider.
  storage: localforage, // or window.sessionStorage or localForage
  modules: ['items'],
  asyncStorage: true,

  restoreState: (key, storage) => storage.getItem(key),
  saveState: (key, state, storage) => storage.setItem(key, state),

  // Function that passes the state and returns the state with only the objects you want to store.
  // Function that passes a mutation and lets you decide if it should update the state in localStorage.
  // filter: mutation => (true)
});

/**
 * Add methods to reset each store and all stores at the same time, useful for logout.
 *
 * @see <a href="https://github.com/vuejs/vuex/issues/1118#issuecomment-439471054">Github issues</a>
 * @param mod vuex config
 */
function createResetHandler(mod) {
  if (mod.hasOwnProperty('modules')) {
    Object.keys(mod.modules).forEach(modName => {
      createResetHandler(mod.modules[modName]);
    });
  }

  if (!mod.hasOwnProperty('state')) return;
  if (!mod.hasOwnProperty('mutations')) mod.mutations = {};
  if (!mod.hasOwnProperty('actions')) mod.actions = {};

  if (!mod.mutations.hasOwnProperty('reset')) {
    const _state = Object.assign({}, mod.state);
    mod.mutations.reset = function reset(state) {
      const login = state.login;
      Object.assign(state, _state);
      if (login) {
        state.login = login;
      }
    };
  }

  if (!mod.actions.hasOwnProperty('reset')) {
    mod.actions.reset = {
      root: true,
      handler({ commit }) {
        commit('reset');
      },
    };
  }
}

const config = {
  modules,
  plugins: [vuexLocalStorage.plugin, vuexLocalForage.plugin],
};
createResetHandler(config);
export default new Vuex.Store(config);
