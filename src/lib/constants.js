export const ItemType = {
  None: 0,
  Currency: 1,
  Armor: 2,
  Weapon: 3,
  Message: 7,
  Engram: 8,
  Consumable: 9,
  ExchangeMaterial: 10,
  MissionReward: 11,
  QuestStep: 12,
  QuestStepComplete: 13,
  Emblem: 14,
  Quest: 15,
  Subclass: 16,
  ClanBanner: 17,
  Aura: 18,
  Mod: 19,
  Dummy: 20,
  Ship: 21,
  Vehicle: 22,
  Emote: 23,
  Ghost: 24,
  Package: 25,
  Bounty: 26,
  Wrapper: 27,
};

export const bungieMembershipType = {
  1: { name: 'Xbox', icon: ['fab', 'fa-xbox'], color: '#107C10' },
  2: { name: 'Psn', icon: ['fab', 'fa-playstation'], color: '#003791' },
  3: { name: 'Steam', icon: ['fab', 'fa-steam'], color: '#000000' },
  4: { name: 'Blizzard', icon: ['fab', 'fa-battle-net'], color: '#00B4FF' },
  5: { name: 'Stadia', color: '#CD2640' },
  10: { name: 'Demon' },
  254: { name: 'BungieNext' },
};

export const traitDefinition = {
  "inventory_filtering.bounty": 'Bounty',
  "quest.exotic": 'Exotic Weapons',
  "quest.playlists": 'Playlist Quest',
  "quest.current_release": 'Current Quests',
  "quest.past": 'Old Quests',
  "inventory_filtering.quest": 'Other',
}

export const traitFilter = {
  "inventory_filtering.bounty": (ids) => ids.includes('inventory_filtering.bounty') || ids.includes('item_type.bounty'),
  "quest.exotic": null,
  "quest.playlists": null,
  "quest.current_release": (ids) => ids.includes('quest.current_release') || ids.includes('quest.seasonal'),
  "quest.past": null,
  "inventory_filtering.quest": (ids) => ids.length === 1 && ids.includes('inventory_filtering.quest'),
}
