import Axios from 'axios';
import store from '@/store';
import * as R from 'ramda';

const UserService = Axios.create({
  baseURL: 'https://sub.spiderbiggen.com/pursuit',
  timeout: 15000,
});

UserService.interceptors.request.use(
    config => {
      // Do something before request is sent
      if (store.getters['user/token']) {
        config.headers.authorization = `bearer ${ store.getters['user/token'] }`;
      }
      config.headers['request-source'] = 'app';
      return config;
    },
    error => error,
);

// respone interceptor
UserService.interceptors.response.use(
    (response) => {
      if (response.data.status === 'success') {
        response.data = response.data.data;
      }
      return response;
    },
    error => {
      if (R.path(['response', 'status'], error) === 401) {
        console.error('Unauthorized:', error);
        store.dispatch('user/logout');
      }
      throw error;
    },
);

export default UserService;
