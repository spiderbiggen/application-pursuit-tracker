const POSSIBLE_CHARACTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

export function generateRandomString(length) {
  let text = '';
  for (let i = 0; i < length; i++) {
    text += POSSIBLE_CHARACTERS.charAt(Math.floor(Math.random() * POSSIBLE_CHARACTERS.length));
  }
  return text;
}
