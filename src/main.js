import Vue from 'vue';
import App from './App.vue';
import store from './store';
import router from './router';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';


Vue.component('fa-icon', FontAwesomeIcon);

Vue.config.productionTip = false;
require('@/stylus/main.styl');

Vue.directive('scroll', {
  inserted: function (el, binding) {
    let f = function (evt) {
      if (binding.value(evt, el)) {
        window.removeEventListener('scroll', f)
      }
    };
    window.addEventListener('scroll', f)
  }
});

new Vue({
  store,
  router,
  render: h => h(App),
}).$mount('#app');
